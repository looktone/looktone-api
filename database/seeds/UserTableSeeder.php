<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleUser = Role::where('name', 'User')->first();
        $simpleUser = new User();
        $simpleUser->name = 'Test User';
        $simpleUser->email = 'testuser@test.com';
        $simpleUser->password = Hash::make('123123');
        $simpleUser->save();
        $simpleUser->markEmailAsVerified();
        $simpleUser->roles()->attach($roleUser);

        $roleMaster = Role::where('name', 'Master')->first();
        $masterUser = new User();
        $masterUser->name = 'Test Master';
        $masterUser->email = 'testmaster@test.com';
        $masterUser->password = Hash::make('123123');
        $masterUser->save();
        $masterUser->markEmailAsVerified();
        $masterUser->roles()->attach($roleMaster);
    }
}

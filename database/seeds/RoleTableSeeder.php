<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleUser = new Role();
        $roleUser->name = 'User';
        $roleUser->save();

        $roleMaster = new Role();
        $roleMaster->name = 'Master';
        $roleMaster->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\ServiceCategory;

class ServiceCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceCategory = new ServiceCategory();
        $serviceCategory->name = 'Test cat';
        $serviceCategory->status = 1;
        $serviceCategory->description = 'Simple test category';
        $serviceCategory->save();

        $serviceCategoryChild = new ServiceCategory();
        $serviceCategoryChild->name = 'Test child cat';
        $serviceCategoryChild->status = 1;
        $serviceCategoryChild->parent_id = $serviceCategory->id;
        $serviceCategoryChild->description = 'Simple test child category';
        $serviceCategoryChild->save();
    }
}

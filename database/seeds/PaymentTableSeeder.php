<?php

use Illuminate\Database\Seeder;
use App\Models\Payment;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentCash = new Payment();
        $paymentCash->name = 'Cash';
        $paymentCash->status = 1;
        $paymentCash->save();

        $paymentCard = new Payment();
        $paymentCard->name = 'Card';
        $paymentCard->status = 1;
        $paymentCard->save();
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bookings', function (Blueprint $table) {
            $table->uuid('id')->primary();;
            $table->integer('user_id')->unsigned();
            $table->date('date_from');
            $table->date('date_to');
            $table->integer('service_id')->unsigned();
            $table->integer('status');
            $table->uuid('transaction_id');
            $table->text('comment');
            $table->timestamps();

            $table->foreign('transaction_id')
                ->references('id')->on('transactions');
            $table->foreign('service_id')
                ->references('id')->on('user_services');
            $table->foreign('user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_bookings');
    }
}

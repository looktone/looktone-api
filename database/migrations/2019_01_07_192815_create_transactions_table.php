<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();;
            $table->integer('payment_id')->unsigned();
            $table->integer('total');
            $table->integer('status')->default(0);
            $table->text('description')->nullable();
            $table->integer('payer_id')->unsigned();
            $table->timestamps();

            $table->foreign('payment_id')
                ->references('id')->on('payments');
            $table->foreign('payer_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}

<?php
namespace App\Services;

use App\Models\UserService;
use App\Models\Payment;
use App\Models\User;

class LooktonePaymentsService extends LooktoneBaseService
{
    private $user;
    private $payment;

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function setPayment($payment)
    {
        $this->payment = Payment::where('id', $payment['payment_id'])->first();
        return $this;
    }

    public function attach()
    {
        // add payment to user
        if ($this->exist()) {
            // if payment already exist then return error
            $this->setError('User already add this payment, you can only delete this payment method');
        } else {
            // if payment no-exist then add
            $this->user->payments()->attach($this->payment);
            $this->setResult($this->user->payments);
        }

        return $this;
    }

    public function exist()
    {
        // check if user already add this payment
        return $this->user->hasPayment($this->payment->id);
    }

    public function detach()
    {
        // remove from user current payment
        if ($this->user->payments()->detach($this->payment->id)) {
            $this->setResult([
                'payments' => $this->user->payments
            ]);
        } else {
            $this->setError(['Removing error']);
        }
        return $this;
    }

}
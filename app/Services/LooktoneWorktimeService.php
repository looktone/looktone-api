<?php

namespace App\Services;


use App\Models\UserWorktime;

class LooktoneWorktimeService
{

    protected $workTime = array();
    public $isValid = true;
    protected $owner;
    private $daysToUpdate = array();
    private $daysToCreate = array();

    public function setWorkTime($workTime)
    {
        $this->workTime = $workTime;
        $this->validate();
        return $this;
    }

    public function validate()
    {
        if (is_array($this->workTime) && count($this->workTime) <= 7) {
            foreach ($this->workTime as $day) {
                if (!$this->isValidTimeRange(array($day['time_from'], $day['time_to']))) {
                    $this->makeNotValid();
                }
            }
        } else {
            $this->makeNotValid();
        }
    }


    public function isValidTimeRange($time)
    {
        if (strtotime($time[0]) > strtotime($time[1])) {
            return false;
        }

        return true;
    }

    public function isDayDublicate($days, $dayName)
    {
        $count = 0;
        foreach ($days as $day) {
            if ($dayName === $day['day']) {
                $count++;
            }
        }

        if ($count > 1) {
            return true;
        }

        return false;
    }

    public function setOwner($owerId)
    {
        $this->owner = $owerId;
        if ($this->workTime && is_array($this->workTime) && count($this->workTime) > 0) {
            foreach ($this->workTime as $index => $day) {
                $this->workTime[$index]['user_id'] = $owerId;
            }
        }
        return $this;
    }

    public function deleteAll(){
        UserWorktime::where('user_id', $this->owner)->delete();
        return $this;
    }

    public function save($days = false)
    {
        if ($days) {
            return UserWorktime::insert($days);
        } else {
            return UserWorktime::insert($this->workTime);
        }

    }

    private function makeNotValid()
    {
        $this->isValid = false;
    }

}
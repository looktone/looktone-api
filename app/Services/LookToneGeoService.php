<?php

namespace App\Services;

use App\Models\UserLocation;
use Geocoder\Laravel\Facades\Geocoder as Geocoder;

class LookToneGeoService{

    public $location;

    public function convertToModel($lat, $lng, $location = false){
        $geocoderResult = Geocoder::reverse($lat, $lng)->get()->first()->toArray();
        if ($location){
            $this->location = $location;
        } else {
            $this->location = new UserLocation();
        }
        if ($geocoderResult) {
            $this->location->lat = $geocoderResult['latitude'];
            $this->location->lng = $geocoderResult['longitude'];
            $this->location->country = $geocoderResult['country'];
            $this->location->locality = $geocoderResult['locality'];
            $this->location->street_name = $geocoderResult['streetName'];
            $this->location->street_number = $geocoderResult['streetNumber'];
            $this->location->postal_code = $geocoderResult['streetNumber'];
        }
        return $this;
    }


    public function setModelId($id){
        $this->location->id = $id;
        return $this;
    }

    public function setOwner($id){
        $this->location->user_id = $id;
        return $this;
    }

    public function save(){
        return $this->location->save();
    }

}
<?php

namespace App\Services;

class LooktoneErrorsService
{
    private $errors = array();

    public function hasErrors()
    {
        return count($this->errors) > 0 ? true : false;
    }

    public function appendError($error)
    {
        return array_push($this->errors, $error);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
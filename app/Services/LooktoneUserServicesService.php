<?php

namespace App\Services;

use App\Models\UserService;

class LooktoneUserServicesService extends LooktoneErrorsService
{

    public $user;
    public $service;

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function setService($service)
    {
        if ($service instanceof UserService) {
            $this->service = $service;
        } else {
            $this->service = $this->toModel($service);
        }

        return $this;
    }

    public function toModel($serviceArray)
    {
        $service = new UserService();
        if ($this->user) {
            $service->user_id = $this->user->id;
        }
        $service->price = $serviceArray['price'];
        $service->description = !empty($serviceArray['description']) ? $serviceArray['description'] : null;
        $service->category_id = $serviceArray['category_id'];
        $service->duration = $serviceArray['duration'];
        $service->status = $serviceArray['status'];

        return $service;
    }

    public function add()
    {
        if ($this->service && $this->user) {
            // Let's check has or not this user same service
            if ($this->user->hasService($this->service->category_id)) {
                $this->appendError('User has create this service recently.');
            } else {
                $this->service->save();
            }
        }

        return $this;
    }

    public function update()
    {
        $userService = $this->user->hasService($this->service->category_id);
        if ($userService){
            $userService->price = $this->service->price;
            $userService->status = $this->service->status;
            $userService->description = $this->service->description;
            $userService->duration = $this->service->duration;

            if (!$userService->save()) {
                $this->appendError('Update process error');
            }
            return $this;
        } else {
            $this->add();
        }
    }


}

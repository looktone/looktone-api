<?php
namespace App\Services;

class LooktoneBaseService
{
    private $errors = [];
    private $result;
    public $status = 200;

    protected function setError($error)
    {
        $this->status = 422;
        return array_push($this->errors, $error);
    }

    protected function hasAnyErrors()
    {
        return count($this->errors) > 0;
    }

    protected function getErrors()
    {
        return $this->errors;
    }

    protected function cleanErrors()
    {
        $this->status = 200;
        $this->errors = [];
    }

    protected function setResult($result)
    {
        $this->result = $result;
    }

    protected function getResult()
    {
        return $this->result;
    }

    public function result()
    {
        if ($this->hasAnyErrors()) {
            return [
                'root' => $this->getErrors()
            ];
        } else {
            return $this->getResult();
        }
    }

}
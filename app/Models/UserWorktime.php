<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorktime extends Model
{
    public $table = 'user_worktime';
    public $fillable = ['user_id', 'day', 'time_from', 'time_to'];

    public function user(){
        return $this->hasOne(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'status'];
    protected $fillable = ['description', 'parent_id', 'name', 'status'];
}

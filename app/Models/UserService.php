<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserService extends Model
{
    public function serviceCategory()
    {
        return $this->hasOne(ServiceCategory::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }
}

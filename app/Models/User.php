<?php

namespace App\Models;

use App\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function location()
    {
        return $this->hasOne(UserLocation::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function worktime()
    {
        return $this->hasMany(UserWorktime::class);
    }

    public function services()
    {
        return $this->hasMany(UserService::class);
    }

    public function payments()
    {
        return $this->belongsToMany(Payment::class);
    }

    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || false;
        }
        return $this->hasRole($roles) || false;
    }

    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    public function hasService($categoryId){
        return $this->services()->whereIn('category_id', [$categoryId])->first();
    }

    public function hasPayment($paymentId)
    {
        return $this->payments()->whereIn('payment_id', [$paymentId])->first();
    }

    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }
}

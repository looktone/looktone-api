<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function users()
    {
        $this->belongsToMany(User::class);
    }
}

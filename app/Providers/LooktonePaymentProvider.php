<?php

namespace App\Providers;

use App\Services\LooktonePaymentsService;
use Illuminate\Support\ServiceProvider;

class LooktonePaymentProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\LooktonePaymentsService', function($app){
            return new LooktonePaymentsService();
        });
    }
}

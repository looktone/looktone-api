<?php

namespace App\Providers;

use App\Services\LooktoneWorktimeService;
use Illuminate\Support\ServiceProvider;

class LooktoneWorktimeProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\LooktoneWorktimeService', function($app){
            return new LooktoneWorktimeService();
        });
    }
}

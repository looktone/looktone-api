<?php

namespace App\Providers;

use App\Services\LookToneGeoService;
use Illuminate\Support\ServiceProvider;

class LookToneGeoProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\LookToneGeoService', function($app){
            return new LookToneGeoService();
        });
    }
}

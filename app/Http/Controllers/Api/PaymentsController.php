<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\DeleteUserPaymentRequest;
use App\Services\LooktonePaymentsService;
use App\Http\Requests\Api\CreateUserPaymentRequest;

class PaymentsController extends ApiController
{
    public function postUserPayment(
        CreateUserPaymentRequest $request,
        LooktonePaymentsService $paymentsService
    )
    {
        $attacing = $paymentsService
            ->setUser($request->user())
            ->setPayment($request->all())
            ->attach();

        return $this->setStatusCode($attacing->status)
                ->response($attacing->result());
    }

    public function deleteUserPayment(
        DeleteUserPaymentRequest $request,
        LooktonePaymentsService $paymentsService
    )
    {
        $detaching = $paymentsService
            ->setUser($request->user())
            ->setPayment($request->all())
            ->detach();

        return $this->setStatusCode($detaching->status)
                ->response($detaching->result());
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateProfileRequest;
use App\Http\Requests\Api\CreateUserLocationRequest;
use App\Http\Requests\Api\CreateWorktimeRequest;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\Http\Controllers\Api\ApiController;
use App\Models\UserProfile;
use App\Services\LookToneGeoService;
use App\Services\LooktoneWorktimeService;
use Geocoder\Laravel\Facades\Geocoder as Geocoder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;
use Validator;

class UserController extends ApiController
{
    public function login(LoginRequest $request)
    {
        if (Auth::attempt([
            'email' => request('email'),
            'password' => request('password')
        ])) {
            $user = Auth::user();
            $token = $user->createToken('MyApp')->accessToken;
            return $this->successResponse([
                'token' => $token,
                'user' => $user
            ]);
        } else {
            return $this->errorValidationResponse('User is invalid');
        }
    }

    public function register(RegisterRequest $request)
    {
        $roleUser = Role::where('name', 'User')->first();
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->roles()->attach($roleUser);
        $token = $user->createToken('MyApp')->accessToken;
        return $this->setStatusCode(201)->successResponse([
            'token' => $token,
            'user' => $user
        ]);
    }

    public function registerMaster(RegisterRequest $request)
    {
        $roleMaster = Role::where('name', 'Master')->first();
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->roles()->attach($roleMaster);
        $token = $user->createToken('MyApp')->accessToken;
        return $this->setStatusCode(201)->successResponse([
            'token' => $token,
            'user' => $user
        ]);
    }

    public function createProfile(CreateProfileRequest $request)
    {
        $data = $request->all();
        $user = $request->user();
        if ($user->profile()->exists()) {
            $userProfile = $user->profile;
        } else {
            $userProfile = new UserProfile();
        }
        $userProfile->user_id = $user->id;
        if (!empty($data['facebook'])) {
            $userProfile->facebook = $data['facebook'];
        }

        if (!empty($data['instagram'])) {
            $userProfile->instagram = $data['instagram'];
        }

        if (!empty($data['google'])) {
            $userProfile->facebook = $data['google'];
        }


        if ($userProfile->save()) {
            return $this->setStatusCode(200)->successResponse([
                'profile' => $user->profile
            ]);
        } else {
            return $this->setStatusCode(422)->errorValidationResponse([
                'root' => ['Can not save user profile data']
            ]);
        }
    }

    public function createLocation(
        CreateUserLocationRequest $request,
        LookToneGeoService $geoService
    )
    {
        $data = $request->all();
        $user = $request->user();

        if ($user->location()->exists()) {
            $result = $geoService->convertToModel($data['lat'], $data['lng'], $user->location)->save();
        } else {
            $result = $geoService->convertToModel($data['lat'], $data['lng'], false)->setOwner($user->id)->save();
        }

        if ($result) {
            return $this->setStatusCode(200)->successResponse([
                'location' => $user->location
            ]);
        } else {
            return $this->setStatusCode(422)->errorValidationResponse([
                'root' => ['Can not save user location']
            ]);
        }
    }

    public function createWorktime(
        CreateWorktimeRequest $request,
        LooktoneWorktimeService $worktimeService
    )
    {
        $workTimeData = $request->all();
        $user = $request->user();
        $workTime = $worktimeService->setWorkTime($workTimeData['days']);


        if ($workTime->isValid) {
            if ($user->worktime()->exists()) {
                //if user worktime already exist then update selected only
                $result = $workTime->setOwner($user->id)->deleteAll()->save();
            } else {
                //if user create worktime first time, them simply create all
                $result = $workTime->setOwner($user->id)->save();
            }

            if ($result) {
                return $this->setStatusCode(201)->successResponse([
                    'worktime' => $user->worktime
                ]);
            } else {
                return $this->setStatusCode(500)->errorValidationResponse([
                    'root' => ['Server update error']
                ]);
            }
        } else {
            return $this->setStatusCode(422)->errorValidationResponse([
                'root' => ['Work time is invalid']
            ]);
        }
    }
}

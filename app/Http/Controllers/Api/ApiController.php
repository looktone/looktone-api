<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $statusCode = 200;

    public function setStatusCode($statusCode){
        $this->statusCode = $statusCode;
        return $this;
    }

    public function errorValidationResponse($errors){
        return $this->response([
            'errors' => $errors
        ]);
    }

    public function successResponse($data){
        return $this->response($data);
    }

    public function response($data) {
        return response()->json([
            'statusCode' => $this->statusCode,
            'data' => $data
        ], $this->statusCode);
    }
}

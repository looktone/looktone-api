<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateUserServiceRequest;
use App\Models\ServiceCategory;
use App\Services\LooktoneUserServicesService;
use Illuminate\Http\Request;

class ServiceController extends ApiController
{
    public function getServicesCategories()
    {
        return $this->successResponse([
            'categories' => ServiceCategory::all()
        ]);
    }

    public function postUserService(
        CreateUserServiceRequest $request,
        LooktoneUserServicesService $userServicesService
    )
    {
        $user = $request->user();
        $action = $userServicesService->setUser($user)->setService($request->all())->add();
        if (!$action->hasErrors()) {
            return $this->setStatusCode(201)->successResponse([
                'services' => $user->services
            ]);
        } else {
            return $this->errorValidationResponse([
                'root' => $action->getErrors()
            ]);
        }
    }

    public function updateUserService(
        CreateUserServiceRequest $request,
        LooktoneUserServicesService $userServicesService
    )
    {
        $user = $request->user();
        $action = $userServicesService->setUser($user)->setService($request->all())->update();
        if (!$action->hasErrors()) {
            return $this->successResponse([
                'services' => $user->services
            ]);
        } else {
            return $this->errorValidationResponse([
                'root' => $action->getErrors()
            ]);
        }
    }

    // TODO: create delete method when will be ready appoiments
    public function deleteUserService()
    {

    }
}

<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiFormRequest;

class CreateWorktimeRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'days' => 'required',
            'days.*.day' => 'required|distinct|date_format:"l"',
            'days.*.time_from' => 'required|date_format:"H:i:s"',
            'days.*.time_to' => 'required|date_format:"H:i:s'
        ];
    }
}

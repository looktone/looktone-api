<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', 'Api\UserController@login');
        Route::post('register', 'Api\UserController@register');
        Route::post('register/master', 'Api\UserController@registerMaster');
    });

    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::post('user/profile', 'Api\UserController@createProfile');
        Route::post('user/location', 'Api\UserController@createLocation');
        Route::post('user/worktime', 'Api\UserController@createWorktime');

        Route::group([], function () {
            Route::post('user/services', 'Api\ServiceController@postUserService');
            Route::patch('user/services', 'Api\ServiceController@updateUserService');
        });

        // Booking endpoints
        Route::post('bookings', 'Api\BookingController@postBooking');
        Route::patch('bookings', 'Api\BookingController@patchBooking');

        // Payments endpoints
        Route::post('payments', 'Api\PaymentsController@postUserPayment');
        Route::delete('payments', 'Api\PaymentsController@deleteUserPayment');
    });

    // Routes for non-auth users
    Route::group([], function () {
        Route::get('services/categories', 'Api\ServiceController@getServicesCategories');
    });
});

